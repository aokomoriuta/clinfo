#pragma warning(push, 1)
#pragma warning(disable: 4996)
#include <iostream>
#include <map>
#include <boost/algorithm/string.hpp>
#include <CL/cl.hpp>
#pragma warning(pop)

template <cl_device_info name, typename T>
static inline T GetInfo(const cl::Device& device)
{
	T val;
	device.getInfo(name, &val);
	return val;
}


template <typename T>
static void ShowBitfield(const std::map<T, std::string> dictionary, const T& val)
{
	if (val == 0)
	{
		std::cout << "None";
	}
	else
	{
		T v = val;
		uint64_t flag = 1;
		while (v != 0)
		{
			if ((val&flag) != 0)
			{
				if (v != val)
				{
					std::cout << ", ";
				}
				std::cout << dictionary.at(flag);
				v &= ~flag;
			}
			flag <<= 1;
		}
	}
}

int main()
{
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	std::cout << "== Platforms == " << std::endl;
	std::cout << "num: " << platforms.size() << std::endl;
	for (const auto& platform : platforms)
	{
		std::cout << "=== Platform : " << platform() << " ===" << std::endl;
		std::cout <<
			"Name    : " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl <<
			"Vendor  : " << platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl <<
			"Version : " << platform.getInfo<CL_PLATFORM_VERSION>() << std::endl <<
			"Profile : " << platform.getInfo<CL_PLATFORM_PROFILE>() << std::endl;

		std::cout << "==== Extensions ====" << std::endl;
		const auto platformExtensionsString = platform.getInfo<CL_PLATFORM_EXTENSIONS>();
		std::list<std::string> platformExtensions;
		boost::split(platformExtensions, platformExtensionsString, boost::is_space());
		for (const auto& platformExtension : platformExtensions)
		{
			if (platformExtension.size() != 0)
			{
				std::cout << "* " << platformExtension << std::endl;
			}
		}

		std::cout << "==== Devices ====" << std::endl;
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
		std::cout << "num: " << devices.size() << std::endl;
		for (const auto& device : devices)
		{
			static const std::map<cl_device_type, std::string> deviceTypes = {
				{ CL_DEVICE_TYPE_CPU, "CPU" },
				{ CL_DEVICE_TYPE_GPU, "GPU" } };
			static const std::map<cl_device_mem_cache_type, std::string> cacheTypes = {
				{ CL_NONE, "No cache" },
				{ CL_READ_ONLY_CACHE, "Read only" },
				{ CL_READ_WRITE_CACHE, "Read & Write" } };
			static const std::map<cl_device_local_mem_type, std::string> localMemTypes = {
				{ CL_NONE, "No local memory" },
				{ CL_LOCAL, "Implemented local" },
				{ CL_GLOBAL, "Same as global" } };
			static const std::map<cl_device_svm_capabilities, std::string> svmCaps = {
				{ CL_DEVICE_SVM_COARSE_GRAIN_BUFFER, "coarse-grain buffer" },
				{ CL_DEVICE_SVM_FINE_GRAIN_BUFFER, "fine-grain buffer" },
				{ CL_DEVICE_SVM_FINE_GRAIN_SYSTEM, "fine-grain system" },
				{ CL_DEVICE_SVM_ATOMICS, "atomic" } };
			static const std::map<cl_command_queue_properties, std::string> queueProps = {
				{ CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, "out-of-order execution" },
				{ CL_QUEUE_PROFILING_ENABLE, "profiling" } };
			std::cout << "===== Device : " << device() << " =====" << std::endl;
			std::cout <<
				"Name                                             : " << device.getInfo<CL_DEVICE_NAME>() << std::endl <<
				"Type                                             : " << deviceTypes.at(device.getInfo<CL_DEVICE_TYPE>()) << std::endl <<
				"Vendor                                           : " << device.getInfo<CL_DEVICE_VENDOR>() << " (ID:" << device.getInfo<CL_DEVICE_VENDOR_ID>() << ")" << std::endl <<
				"Version                                          : " << device.getInfo<CL_DEVICE_VERSION>() << std::endl <<
				"Profile                                          : " << device.getInfo<CL_DEVICE_PROFILE>() << std::endl <<
				"Driver version                                   : " << device.getInfo<CL_DRIVER_VERSION>() << std::endl <<
				"OpenCL C version                                 : " << device.getInfo<CL_DEVICE_OPENCL_C_VERSION>() << std::endl;
			auto version = device.getInfo<CL_DEVICE_VERSION>();
			if (version.find("2.0") == std::string::npos)
			{
				std::cout << "This device is not support OpenCL 2.0" << std::endl;
			}
			else
			{
				std::cout <<
					"Max clock [MHz]                                  : " << device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << std::endl <<
					"Max compute units                                : " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl <<
					"Max size of work group                           : " << device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl <<
					"Max size of work item                            : ";
				{
					const std::size_t maxWorkitemDimension = device.getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>();
					const auto& maxWorkitemSizes = device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
					std::cout << "(" << maxWorkitemSizes[0];
					for (unsigned int i = 1; i < maxWorkitemDimension; i++)
					{
						std::cout << ", " << maxWorkitemSizes[i];
					}
					std::cout << ")" << std::endl;

					std::cout <<
						"Global memory size [GB]                          : " << static_cast<double>(device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()) / 1024 / 1024 / 1024 << std::endl <<
						"Allocatable memory size [GB]                     : " << static_cast<double>(device.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>()) / 1024 / 1024 / 1024 << std::endl <<
						"Global cache type                                : " << cacheTypes.at(device.getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_TYPE>()) << std::endl <<
						"Global memory size [GB]                          : " << static_cast<double>(device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()) / 1024 / 1024 / 1024 << std::endl <<
						"Global cache size [KB]                           : " << static_cast<double>(device.getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_SIZE>()) / 1024 << std::endl <<
						"Global cache line size [byte]                    : " << static_cast<double>(device.getInfo<CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE>()) << std::endl <<
						"Constant buffer size [KB]                        : " << static_cast<double>(device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>()) / 1024 << std::endl <<
						"Max count of kernel constant parameters          : " << device.getInfo<CL_DEVICE_MAX_CONSTANT_ARGS>() << std::endl <<
						"Max size of single global variable [GB]          : " << static_cast<double>(GetInfo<CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE, size_t>(device)) / 1024 / 1024 / 1024 << std::endl <<
						"Preferred total size of global variables [GB]    : " << static_cast<double>(GetInfo<CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE, size_t>(device)) / 1024 / 1024 / 1024 << std::endl <<
						"Local memory type                                : " << localMemTypes.at(device.getInfo<CL_DEVICE_LOCAL_MEM_TYPE>()) << std::endl <<
						"Local memory size [KB]                           : " << static_cast<double>(device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>()) / 1024 << std::endl <<
						"Max size of kernel arguments                     : " << device.getInfo<CL_DEVICE_MAX_PARAMETER_SIZE>() << std::endl <<
						"Error correction                                 : " << ((device.getInfo<CL_DEVICE_ERROR_CORRECTION_SUPPORT>() == CL_TRUE) ? "Yes" : "No") << std::endl <<
						"Shared virtual memory type                       : "; ShowBitfield(svmCaps, GetInfo<CL_DEVICE_SVM_CAPABILITIES, cl_device_svm_capabilities>(device)); std::cout << std::endl <<
						"Preferred atomic alignment for fine-grained SVM  : " << GetInfo<CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT, cl_uint>(device) << std::endl <<
						"Preferred atomic alignment for global            : " << GetInfo<CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT, cl_uint>(device) << std::endl <<
						"Preferred atomic alignment for local             : " << GetInfo<CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT, cl_uint>(device) << std::endl <<
						"Endian                                           : " << ((device.getInfo<CL_DEVICE_ENDIAN_LITTLE>() == CL_TRUE) ? "Little" : "Big") << std::endl <<
						"Default bit size of 'unsigned int'               : " << device.getInfo<CL_DEVICE_ADDRESS_BITS>() << std::endl <<
						"Command queue propeties on host                  : "; ShowBitfield(queueProps, GetInfo<CL_DEVICE_QUEUE_ON_HOST_PROPERTIES, cl_command_queue_properties>(device)); std::cout << std::endl <<
						"Command queue propeties on device                : "; ShowBitfield(queueProps, GetInfo<CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES, cl_command_queue_properties>(device)); std::cout << std::endl <<
						"Max size of device queue                         : " << GetInfo<CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE, cl_uint>(device) << std::endl <<
						"Preferred size of device queue                   : " << GetInfo<CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, cl_uint>(device) << std::endl <<
						"Max count of device queue                        : " << GetInfo<CL_DEVICE_MAX_ON_DEVICE_QUEUES, cl_uint>(device) << std::endl <<
						"Max count of events on device queue              : " << GetInfo<CL_DEVICE_MAX_ON_DEVICE_EVENTS, cl_uint>(device) << std::endl <<
						"Native vector width (char)                       : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR>() << std::endl <<
						"Native vector width (short)                      : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT>() << std::endl <<
						"Native vector width (int)                        : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_INT>() << std::endl <<
						"Native vector width (long)                       : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG>() << std::endl <<
						"Native vector width (float)                      : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT>() << std::endl <<
						"Native vector width (double)                     : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE>() << std::endl <<
						"Native vector width (half)                       : " << device.getInfo<CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF>() << std::endl <<
						"";
				}
			}
		}
	}

	system("pause");
	return 0;
}
